import React, { useState } from "react";
import { Input } from "./Input";

export const TableRow = (props) => {
  const [isDisabled, setIsDisabled] = useState(true);
  const [data, setData] = useState(props.data);

  const isChange = () => {
    setIsDisabled(!isDisabled);
  };

  // Сохранение изменений в строке
  const changeData = (value, index) => {
    let newData = data;
    data[index] = value;
    setData(newData);
  };

  // Передача новых данных в родительский компонент
  const saveData = () => {
    setIsDisabled(!isDisabled);
    return props.saveData(data);
  };

  return (
    <tr>
      {data.map((item, index) => {
        return (
          <td key={index}>
            <Input
              value={item}
              disabled={isDisabled}
              onChange={(value) => changeData(value, index)}
            />
          </td>
        );
      })}
      <td>
        {isDisabled ? (
          <button onClick={isChange}>Edit</button>
        ) : (
          <button onClick={saveData}>Save</button>
        )}
      </td>
      <td>
        <button onClick={props.deleteItem}>Delete</button>
      </td>
    </tr>
  );
};

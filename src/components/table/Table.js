import axios from "axios";
import React, { Component } from "react";
import { TableRow } from "./TableRow";
import AddForm from "./AddForm";

import "./Table.css";

export default class Table extends Component {
  state = {
    isLoading: true,
    data: [],
    headRow: [],
    newItem: {},
  };

  componentDidMount() {
    this.fetchData();
  }

  // Получение данных
  async fetchData() {
    try {
      this.setState({ isLoading: true });
      await axios
        .get("http://178.128.196.163:3000/api/records")
        .then((response) => {
          this.setState({ data: response.data });
          // Инициализация заголовков столбцов
          let headRow = [];
          let newItem = {};
          for (const key in this.state.data[0].data) {
            headRow.push(key);
            newItem[key] = "";
          }
          this.setState({ headRow, newItem, isLoading: false });
        });
    } catch (error) {
      console.log(error);
    }
  }

  // Удаление записи по id
  deleteItem = async (id) => {
    this.setState({ isLoading: true });
    try {
      await axios
        .delete("http://178.128.196.163:3000/api/records/" + id)
        .then(() => {
          this.fetchData();
        });
    } catch (error) {
      console.log(error);
    }
  };

  // Сохранение изменений определенной строчки по id
  // value: ['test', '13']
  saveData = async (id, value) => {
    let newItem;
    let i = 0;
    this.state.data.forEach((item) => {
      if (item._id === id) {
        newItem = item;
        // Передача новых значений объекту data
        for (let key in newItem.data) {
          newItem.data[key] = value[i++];
        }
      }
    });
    try {
      await axios.post(
        "http://178.128.196.163:3000/api/records/" + id,
        newItem
      );
      this.fetchData();
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <>
        {this.state.isLoading ? (
          <h2>Loading...</h2>
        ) : (
          <div className="container">
            <div className="newItemSection">
              <AddForm
                fetchData={this.fetchData.bind(this)}
                items={this.state.newItem}
              />
            </div>
            <p className="textError">{this.state.error}</p>
            <table className="table" border={1}>
              <thead>
                <tr>
                  {this.state.headRow.map((item, index) => (
                    <th key={index}>{item}</th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {this.state.data.map((item, index) => {
                  // Передача значений объекта в фун. компонент
                  let newData = [];
                  for (let key in item.data) {
                    newData.push(item.data[key]);
                  }
                  return (
                    <TableRow
                      key={index}
                      data={newData}
                      saveData={(data) => this.saveData(item._id, data)}
                      deleteItem={() => this.deleteItem(item._id)}
                    />
                  );
                })}
              </tbody>
            </table>
          </div>
        )}
      </>
    );
  }
}

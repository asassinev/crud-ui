import React, { useState } from "react";

export const Input = (props) => {
  const [value, setValue] = useState(props.value);

  const changeHandler = (event) => {
    setValue(event.target.value);
    return props.onChange(event.target.value);
  };

  return (
    <input
      type="text"
      disabled={props.disabled || false}
      onChange={changeHandler}
      value={value}
    />
  );
};

import React from "react";
import axios from "axios";
import { Input } from "./Input";

export default class AddForm extends React.Component {
  state = {
    data: this.props.items,
    newItem: [],
  };

  componentDidMount() {
    // parse object into array
    let newItem = [];
    for (const key in this.state.data) {
      newItem.push({ title: key, data: this.state.data[key] });
    }
    this.setState({ newItem });
  }

  // Добавление новой записи
  async addNewItem() {
    let newItem = {};
    this.state.newItem.forEach((item) => {
      newItem[item.title] = item.data;
    });
    try {
      await axios
        .put("http://178.128.196.163:3000/api/records", {
          data: newItem,
        })
        .then(() => {
          return this.props.fetchData();
        });
    } catch (error) {
      console.log(error);
    }
  }

  // Изменение значений у нового объекта
  changeHandler = (value, index) => {
    let newItem = this.state.newItem;
    newItem[index].data = value;
    this.setState({ newItem });
    console.log(this.state.newItem);
  };

  render() {
    return (
      <>
        {this.state.newItem.map((item, index) => {
          return (
            <label key={index}>
              {item.title}
              <Input
                type="text"
                onChange={(value) => this.changeHandler(value, index)}
                value={item.data}
              />
            </label>
          );
        })}
        <button className="btn" onClick={this.addNewItem.bind(this)}>
          Add new item
        </button>
      </>
    );
  }
}
